/*
 * Copyright 2015 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.Set;

/**
 * Metadata for a configurable property.
 *
 * @author kblair
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PropertyMetadata {

    private String name;
    private String description;
    private String defaultValue;
    private boolean required;
    private String type;
    private Set<String> enumConstants;
    @JsonIgnore
    private Class<?> clazz;
    @JsonIgnore
    private Field field;
    @JsonIgnore
    private boolean isDelegate;

    /**
     * The property name.
     * <p>
     * Property names should be unique for a given bean.  Generally they should match the field name.
     * </p>
     *
     * @return The property name.
     */
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * A property description.
     * <p>
     * Use to document the property and expected values.
     * </p>
     *
     * @return The property description.
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * The default value for the property when one is not provided.
     * <p>
     * A default value implies the property is not required.
     * </p>
     *
     * @return The default value of the property.
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
        if (defaultValue != null && !defaultValue.isEmpty()) {
            this.required = false;
        }
    }

    /**
     * Test if this property has a default value.
     *
     * @return True if there is a default value, false if not.
     */
    public boolean hasDefaultValue() {
        return defaultValue != null;
    }

    /**
     * True if the property is required, false if it is optional.
     * <p>
     * A property with a default value is implicitly not required.  A property that has no default value and is
     * explicitly set to not required will not set at all.
     * </p>
     *
     * @return
     */
    public boolean isRequired() {
        return required;
    }

    public void setRequired(final boolean required) {
        this.required = required;
        if (required) {
            defaultValue = null;
        }
    }

    /**
     * The expected type of the property.
     *
     * @return The type.
     */
    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    /**
     * The accepted enum values, if the expected type is an enum.
     *
     * @return The enum values to accept.
     */
    public Set<String> getEnumConstants() {
        return enumConstants;
    }

    public void setEnumConstants(final Set<String> enumConstants) {
        this.enumConstants = enumConstants;
    }

    Class<?> getClazz() {
        return clazz;
    }

    void setClazz(final Class<?> clazz) {
        this.clazz = clazz;
    }

    Field getField() {
        return field;
    }

    void setField(final Field field) {
        this.field = field;
    }

    boolean isDelegate() {
        return isDelegate;
    }

    void setDelegate(boolean delegate) {
        isDelegate = delegate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyMetadata)) return false;
        PropertyMetadata that = (PropertyMetadata) o;
        return isRequired() == that.isRequired() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getDefaultValue(), that.getDefaultValue()) &&
                Objects.equals(getType(), that.getType());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getDescription(), getDefaultValue(), isRequired(), getType());
    }

    @Override
    public String toString() {
        return "PropertyMetadata{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", defaultValue='" + defaultValue + '\'' +
                ", required=" + required +
                ", type=" + type +
                '}';
    }
}
