/*
 * Copyright 2015 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Metadata for configurable bean.
 *
 * @author kblair
 */
public class BeanMetadata {

    private String name;
    private String description;
    private List<PropertyMetadata> properties;

    /**
     * Construct empty metadata, used by frameworks primarily.
     */
    public BeanMetadata() {
    }

    /**
     * Construct metadata with the specified name and description and zero properties.
     *
     * @param name        The bean name.
     * @param description The bean description.
     */
    public BeanMetadata(final String name, final String description) {
        this.name = name;
        this.description = description;
        this.properties = new ArrayList<>();
    }

    /**
     * Construct metadata with the specified values.
     *
     * @param name        The bean name.
     * @param description The bean description.
     * @param properties  The configurable properties for the bean.
     */
    public BeanMetadata(final String name, final String description, final List<PropertyMetadata> properties) {
        this.name = name;
        this.description = description;
        this.properties = new ArrayList<>(properties); // defensive copy
    }

    /**
     * The bean name.
     * <p>
     * Bean names may be expected to be unique, depending on the usage.  For example it is expected that in a Spring
     * application the name will correspond to the name of the bean in Spring.
     * </p>
     *
     * @return The bean name.
     */
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    /**
     * A bean description.
     * <p>
     * Use to document the bean.
     * </p>
     *
     * @return The bean description.
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Configurable properties for the bean.
     *
     * @return The bean properties.
     */
    public List<PropertyMetadata> getProperties() {
        return properties;
    }

    public void setProperties(final List<PropertyMetadata> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "BeanMetadata{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", properties=" + properties +
                '}';
    }
}
