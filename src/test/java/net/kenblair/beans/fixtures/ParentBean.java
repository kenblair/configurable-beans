package net.kenblair.beans.fixtures;

import net.kenblair.beans.annotations.ConfigurableProperty;

public class ParentBean {

    public static final String NAME_DESC = "Parent's name";

    @ConfigurableProperty(description = NAME_DESC)
    private String name;
}
