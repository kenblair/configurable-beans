/*
 * Copyright 2018 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans;

import net.kenblair.beans.annotations.ConfigurableBean;
import net.kenblair.beans.annotations.ConfigurableProperty;
import net.kenblair.beans.annotations.ConfiguredBean;
import net.kenblair.beans.annotations.DateFormat;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.apache.commons.configuration2.MapConfiguration;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;
import static org.reflections.ReflectionUtils.getFields;
import static org.reflections.ReflectionUtils.withAnnotation;

/**
 * Provides methods for documenting and configuring properties in a bean at run time.
 *
 * @author Ken Blair
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class ConfigurableBeans {

    static DateTimeFormatter DEFAULT_DATE_TIME_FORMAT = new DateTimeFormatterBuilder()
            .parseLenient().parseCaseInsensitive()
            .appendOptional(DateTimeFormatter.ISO_DATE_TIME)
            .appendOptional(DateTimeFormatter.ISO_DATE)
            .parseDefaulting(ChronoField.OFFSET_SECONDS, 0)
            .toFormatter();

    /**
     * Configure the provided bean utilizing {@link Configurable} if implemented and injection if not.
     *
     * @param bean       The bean to configure.
     * @param properties The properties to configure the bean with.
     * @return The configured bean, the same instance as provided, for convenience.
     */
    public static void configure(final Object bean, final Map<String, ?> properties) {
        if (bean instanceof Configurable) {
            configureBean((Configurable<?>) bean, properties);
        } else {
            injectBean(bean, properties);
        }
    }

    /**
     * Convenience method to configure a bean by providing a {@link Configuration} instead of a {@link Map}.
     * <p>
     * Identical to {@link #configure(Object, Map)}.
     * </p>
     *
     * @param bean          The bean to configure.
     * @param configuration The properties to configure the bean with.
     * @see #configure(Object, Map)
     */
    public static void configure(final Object bean, final Configuration configuration) {
        configure(bean, convertConfiguration(configuration));
    }

    /**
     * Convenience method to configure a bean by providing a {@link ImmutableConfiguration} instead of a {@link Map}.
     * <p>
     * Identical to {@link #configure(Object, Map)}.
     * </p>
     *
     * @param bean          The bean to configure.
     * @param configuration The properties to configure the bean with.
     * @see #configure(Object, Map)
     */
    public static void configure(final Object bean, final ImmutableConfiguration configuration) {
        configure(bean, convertImmutableConfiguration(configuration));
    }

    /**
     * Perform field injection on the provided bean using the provided properties.
     *
     * @param <T>        The type of the bean to inject.
     * @param bean       The bean to inject.
     * @param properties The properties to use.
     * @return The injected bean, the same instance as provided, for convenience.
     */
    public static <T> void injectBean(final T bean, final Map<String, ?> properties) {
        injectConfiguredBeans(bean, properties);
        final Collection<PropertyMetadata> metadata = getBeanProperties(bean.getClass());
        for (final PropertyMetadata property : metadata) {
            if (!property.isDelegate()) {
                injectProperty(bean, property, properties.get(property.getName()));
            }
        }
        initializeBean(bean);
    }

    private static void injectConfiguredBeans(final Object bean, final Map<String, ?> properties) {
        for (final Field field : findConfiguredBeans(bean.getClass())) {
            final Object delegate;
            final boolean accessible = field.isAccessible();
            try {
                if (!accessible) {
                    field.setAccessible(true);
                }
                if (field.get(bean) == null) {
                    delegate = field.getType().newInstance();
                    field.set(bean, delegate);
                } else {
                    delegate = field.get(bean);
                }
            } catch (IllegalAccessException e) {
                throw new IllegalArgumentException("Could not access field " + field.getName() + " on bean " + bean);
            } catch (InstantiationException e) {
                throw new IllegalArgumentException("Could not create a new instance for field " + field.getName() + " on bean " + bean);
            } finally {
                if (!accessible) {
                    field.setAccessible(false);
                }
            }
            for (final PropertyMetadata property : getBeanProperties(field.getType())) {
                injectProperty(delegate, property, properties.get(property.getName()));
            }
        }
    }

    private static List<Field> findConfiguredBeans(final Class<?> clazz) {
        final List<Field> configuredBeans = new ArrayList<>();
        if (clazz.getSuperclass() != null) {
            configuredBeans.addAll(findConfiguredBeans(clazz.getSuperclass()));
        }
        configuredBeans.addAll(getFields(clazz, withAnnotation(ConfiguredBean.class)));
        return configuredBeans;
    }

    /**
     * Configures a bean using the {@link Configurable} interface.
     *
     * @param <C>        The type of the configuration object.
     * @param <T>        The type of the bean implementing Configurable.
     * @param bean       The bean to configure.
     * @param properties The properties to configure the bean with.
     * @return The bean after performing configuration.
     */
    public static <C, T extends Configurable<C>> void configureBean(final T bean, final Map<String, ?> properties) {
        try {
            final Class<C> configClazz = discoverConfigurationType(bean.getClass());
            final C configuration = buildConfiguration(configClazz, getBeanProperties(bean.getClass()), properties);
            bean.configure(configuration);
        } catch (final IllegalAccessException | InstantiationException e) {
            throw new IllegalStateException("Failed to build configuration for " + bean, e);
        }
        initializeBean(bean);
    }

    /**
     * Build a configuration object of the specified type.
     *
     * @param type       The type of the configuration to build.
     * @param properties The properties to configure.
     * @param <T>        The type of the configuration.
     * @return The configuration.
     * @throws RuntimeException If the configuration cannot be instantiated or injected.
     */
    public static <T> T buildConfiguration(final Class<T> type, final Map<String, ?> properties) {
        try {
            return buildConfiguration(type, getBeanProperties(type), properties);
        } catch (final InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Failed to create configuration.", e);
        }
    }

    /**
     * Convenience method for {@link #buildConfiguration(Class, Map)} that converts from {@link ImmutableConfiguration}
     * automatically.
     *
     * @param type          The type of the configuration to build.
     * @param configuration The properties to configure.
     * @param <T>           The type of the configuration.
     * @return The configuration.
     * @throws RuntimeException If the configuration cannot be instantiated or injected.
     */
    public static <T> T buildConfiguration(final Class<T> type, final ImmutableConfiguration configuration) {
        return buildConfiguration(type, convertImmutableConfiguration(configuration));
    }

    /**
     * Convenience method for {@link #buildConfiguration(Class, Map)} that converts from {@link Configuration}
     * automatically.
     *
     * @param type          The type of the configuration to build.
     * @param configuration The properties to configure.
     * @param <T>           The type of the configuration.
     * @return The configuration.
     * @throws RuntimeException If the configuration cannot be instantiated or injected.
     */
    public static <T> T buildConfiguration(final Class<T> type, final Configuration configuration) {
        return buildConfiguration(type, convertConfiguration(configuration));
    }

    /**
     * Convert an {@link ImmutableConfiguration} to a {@link Map}.
     *
     * @param configuration The configuration to convert.
     * @return The configuration as a map.
     */
    private static Map<String, ?> convertImmutableConfiguration(final ImmutableConfiguration configuration) {
        final Map<String, Object> properties = new HashMap<>();
        for (final Iterator<String> it = configuration.getKeys(); it.hasNext(); ) {
            final String key = it.next();
            properties.put(key, configuration.getProperty(key));
        }
        return properties;
    }

    /**
     * Convert a {@link Configuration} to a {@link Map}.
     *
     * @param configuration The configuration to convert.
     * @return The configuration as a map.
     */
    private static Map<String, ?> convertConfiguration(final Configuration configuration) {
        final Map<String, Object> properties = new HashMap<>();
        for (final Iterator<String> it = configuration.getKeys(); it.hasNext(); ) {
            final String key = it.next();
            properties.put(key, configuration.getProperty(key));
        }
        return properties;
    }

    /**
     * Find the type of configuration expected by the class.
     *
     * @param clazz The class to find a configuration type for.
     * @return The configuration type.
     */
    static Class discoverConfigurationType(final Class clazz) {
        for (final Type anInterface : clazz.getGenericInterfaces()) {
            if (anInterface instanceof ParameterizedType && anInterface.getTypeName().startsWith(Configurable.class.getTypeName())) {
                final Type type = ((ParameterizedType) anInterface).getActualTypeArguments()[0];
                if (type instanceof Class) {
                    return (Class) type;
                }
            }
        }
        if (clazz.getSuperclass() != null) {
            return discoverConfigurationType(clazz.getSuperclass());
        }
        throw new IllegalStateException("Could not find configuration type.");
    }

    /**
     * Builds a configuration object from the provided properties.
     *
     * @param clazz      The class of the expected configuration object.
     * @param metadata   The property metadata for the class the configuration is for.
     * @param properties The properties to inject.
     * @param <T>        The configuration type.
     * @return The configuration object.
     * @throws IllegalAccessException If the constructor is not accessible.
     * @throws InstantiationException If instantiation fails for some reason.
     */
    static <T> T buildConfiguration(final Class<T> clazz,
                                    final List<PropertyMetadata> metadata,
                                    final Map<String, ?> properties)
            throws InstantiationException, IllegalAccessException {
        // handle commons configuration
        if (clazz.equals(org.apache.commons.configuration2.Configuration.class)
                || clazz.equals(ImmutableConfiguration.class)) {
            return (T) new MapConfiguration(addDefaultsAndValidate(metadata, properties));
        } else if (clazz.equals(Configuration.class)) {
            return (T) new org.apache.commons.configuration.MapConfiguration(addDefaultsAndValidate(metadata, properties));
        }

        // generic object injection
        final T configuration = clazz.newInstance();
        injectBean(configuration, properties);
        return configuration;
    }

    /**
     * Adds any missing properties that have a default throwing an exception if a missing property is required.
     *
     * @param metadata   The property metadata.
     * @param properties The property values provided.
     * @return The calculated properties including defaults for unset properties.
     */
    static Map<String, ?> addDefaultsAndValidate(final List<PropertyMetadata> metadata, final Map<String, ?> properties) {
        // add the default value for any missing properties and fail if a required property is not present
        final Map<String, Object> calculatedProperties = new HashMap<>(metadata.size());
        for (final PropertyMetadata property : metadata) {
            if (properties.containsKey(property.getName())) {
                calculatedProperties.put(property.getName(), properties.get(property.getName()));
            } else if (property.hasDefaultValue()) {
                calculatedProperties.put(property.getName(), property.getDefaultValue());
            } else if (property.isRequired()) {
                throw new IllegalArgumentException("Property " + property.getName() + " is required");
            }
        }
        return calculatedProperties;
    }

    /**
     * Initialize a bean calling {@link Initializable#initializationFailure(Exception)} if an exception is thrown.
     * <p>
     * Should only be called after a bean has been configured/injected with properties.
     * </p>
     *
     * @param bean The bean to initialize.
     */
    static void initializeBean(final Object bean) {
        if (bean instanceof Initializable) {
            try {
                ((Initializable) bean).initialize();
            } catch (final Exception e) {
                // note: any exception in initializationFailure will just end up propagating up the stack
                ((Initializable) bean).initializationFailure(e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Converts all properties annotated with {@link ConfigurableProperty} on the object to a {@link Map}.
     *
     * @param o The object to get properties from.
     * @return A map of properties
     * @throws SecurityException If a field is inaccessible and the request to make it accessible is denied.
     */
    public static Map<String, ?> buildPropertiesFrom(final Object o) {
        final Map<String, Object> properties = new HashMap<>();
        for (final PropertyMetadata property : getBeanProperties(o.getClass())) {
            final Field field = property.getField();
            final boolean accessible = field.isAccessible();
            if (!accessible) {
                field.setAccessible(true);
            }
            try {
                Object value = field.get(o);
                if (value == null && property.hasDefaultValue()) {
                    value = property.getDefaultValue();
                }
                if (value != null) {
                    properties.put(property.getName(), value);
                } else if (property.isRequired()) {
                    throw new IllegalArgumentException("Property " + property.getName() + " is required");
                }
            } catch (final IllegalAccessException e) {
                // this should never happen
            } finally {
                if (!accessible) {
                    field.setAccessible(false);
                }
            }
        }
        return properties;
    }

    /**
     * Inject the property into the bean using the specified value.
     * <p>
     * If a value is provided it will be used, if not the default value will be used.  If there is no value provided and
     * no default value specified and the field is required a {@link IllegalArgumentException} will be thrown.
     * </p>
     *
     * @param bean     The bean to inject.
     * @param metadata Property metadata for the field.
     * @param value    The value to inject or {@code null} if there is no value.
     * @throws IllegalArgumentException If the property is required and no value has been provided or if the provided
     *                                  property cannot be converted.
     * @throws SecurityException        If the field is inaccessible and the request to make it accessible is denied.
     */
    static void injectProperty(final Object bean, final PropertyMetadata metadata, final Object value) {
        final Field field = metadata.getField();
        if (!field.getDeclaringClass().isAssignableFrom(bean.getClass())) {
            throw new IllegalArgumentException("Property " + metadata.getName() + " was imported but cannot be set on bean " + bean.getClass());
        }
        if (Modifier.isFinal(field.getModifiers())) {
            throw new IllegalArgumentException("Injection of fields marked final is not supported");
        }
        if (metadata.isRequired() && value == null) {
            throw new IllegalArgumentException("Property " + metadata.getName() + " is required");
        }
        final boolean accessible = field.isAccessible();
        if (!accessible) {
            field.setAccessible(true);
        }
        try {
            if (value != null && !value.toString().isEmpty()) {
                injectProperty(bean, field, metadata.getClazz(), value);
            } else if (metadata.hasDefaultValue()) {
                injectProperty(bean, field, metadata.getClazz(), metadata.getDefaultValue());
            } else if (metadata.isRequired()) {
                throw new IllegalArgumentException("Property " + metadata.getName() + " is required");
            }
        } catch (final IllegalAccessException e) {
            // this should never happen because we already set it accessible
            throw new IllegalStateException("Failed to inject property " + metadata.getName(), e);
        } catch (final IllegalArgumentException e) {
            throw new IllegalArgumentException("Failed to inject property " + metadata.getName(), e);
        } finally {
            if (!accessible) {
                field.setAccessible(false);
            }
        }
    }

    /**
     * Attempts to inject a value into a field for the provided bean converting to the correct type if possible.
     *
     * @param bean  The bean to inject.
     * @param field The field to inject for.
     * @param type  The desired type of the field.
     * @param value The value to inject.
     * @throws IllegalAccessException   If the field is inaccessible.
     * @throws IllegalArgumentException If the value cannot be converted to the property's type.
     */
    static void injectProperty(final Object bean, final Field field, final Class<?> type, final Object value)
            throws IllegalAccessException {
        if (type.isAssignableFrom(value.getClass())) {
            // if it's already assignable it's easy
            field.set(bean, value);
        } else if (type == String.class) {
            // anything can be converted to a String
            field.set(bean, value.toString());
        } else if (type.isPrimitive() || Number.class.isAssignableFrom(type)) {
            // handle conversion to primitives from wrappers or a String
            if (type == Integer.TYPE || type == Integer.class) {
                field.set(bean, value instanceof Integer ? value : Integer.valueOf(value.toString()));
            } else if (type == Boolean.TYPE || type == Boolean.class) {
                field.set(bean, value instanceof Boolean ? value : Boolean.valueOf(value.toString()));
            } else if (type == Long.TYPE || type == Long.class) {
                field.set(bean, value instanceof Long ? value : Long.valueOf(value.toString()));
            } else if (type == Short.TYPE || type == Short.class) {
                field.set(bean, value instanceof Short ? value : Short.valueOf(value.toString()));
            } else if (type == Double.TYPE || type == Double.class) {
                field.set(bean, value instanceof Double ? value : Double.valueOf(value.toString()));
            } else if (type == Float.TYPE || type == Float.class) {
                field.set(bean, value instanceof Float ? value : Float.valueOf(value.toString()));
            } else if (type == Character.TYPE || type == Character.class) {
                if (value instanceof Character) {
                    field.set(bean, value);
                } else if (value instanceof String && ((String) value).length() == 1) {
                    field.set(bean, ((String) value).charAt(0));
                } else {
                    throw new IllegalArgumentException("Property requires a Character or a String where length = 1");
                }
            }
        } else if (type.isEnum()) {
            field.set(bean, Enum.valueOf((Class<? extends Enum>) type, value.toString()));
        } else if (Date.class.isAssignableFrom(type)) {
            if (TemporalAccessor.class.isAssignableFrom(value.getClass())) {
                field.set(bean, Date.from(Instant.from((TemporalAccessor) value)));
            } else {
                field.set(bean, Date.from(Instant.from(convertToTemporal(field, value.toString()))));
            }
        } else if (String[].class.isAssignableFrom(type)) {
            if (String.class.isAssignableFrom(value.getClass())) {
                field.set(bean, new String[]{value.toString()});
            } else if (Iterable.class.isAssignableFrom(value.getClass())) {
                final List<String> values = new ArrayList<>();
                for (final Object o : (Iterable) value) {
                    values.add(value.toString());
                }
                field.set(bean, values.toArray(new String[0]));
            } else if (Iterator.class.isAssignableFrom(value.getClass())) {
                final List<String> values = new ArrayList<>();
                for (Iterator itr = (Iterator) values; itr.hasNext(); ) {
                    final Object o = itr.next();
                    values.add(o.toString());
                }
                field.set(bean, values.toArray(new String[0]));
            }
        } else if (Temporal.class.isAssignableFrom(type)) {
            field.set(bean, convertToTemporal(field, value.toString()));
        } else {
            // TODO: add an option to use a "Converter" that clients can give to us to handle mappings
            // TODO: using Orika bean mapper might be good for a default conversion option
            throw new IllegalArgumentException("Unsupported conversion: from " + value.getClass() + " to " + field.getType() + " for " + type);
        }
    }

    @SuppressWarnings("Duplicates")
    // code is technically not a duplicate, the two classes share a method name but no interfaces
    private static TemporalAccessor convertToTemporal(final Field field, final String value)
            throws IllegalAccessException {
        final DateFormat dateFormat = field.getAnnotation(DateFormat.class);
        if (dateFormat != null && dateFormat.custom() != CustomDateFormat.class) {
            final CustomDateFormat formatter;
            try {
                formatter = dateFormat.custom().newInstance();
            } catch (final InstantiationException e) {
                throw new IllegalStateException("Could not create a custom formatter for field " + field.getName(), e);
            }
            if (Date.class.isAssignableFrom(field.getType())) {
                return Instant.from(formatter.parse(value));
            } else if (Instant.class.isAssignableFrom(field.getType())) {
                return Instant.from(formatter.parse(value));
            } else if (OffsetDateTime.class.isAssignableFrom(field.getType())) {
                return OffsetDateTime.from(formatter.parse(value));
            } else if (ZonedDateTime.class.isAssignableFrom(field.getType())) {
                return ZonedDateTime.from(formatter.parse(value));
            } else if (LocalDateTime.class.isAssignableFrom(field.getType())) {
                return LocalDateTime.from(formatter.parse(value));
            } else if (LocalDate.class.isAssignableFrom(field.getType())) {
                return LocalDate.from(formatter.parse(value));
            } else if (LocalTime.class.isAssignableFrom(field.getType())) {
                return LocalTime.from(formatter.parse(value));
            } else if (OffsetTime.class.isAssignableFrom(field.getType())) {
                return OffsetTime.from(formatter.parse(value));
            }
        } else {
            final DateTimeFormatter formatter;
            if (dateFormat != null && !dateFormat.value().equals("\n\t\t\n\t\t\n\uE000\uE001\uE002\n\t\t\t\t\n")) {
                formatter = DateTimeFormatter.ofPattern(dateFormat.value());
            } else {
                formatter = DEFAULT_DATE_TIME_FORMAT;
            }
            if (Date.class.isAssignableFrom(field.getType())) {
                return Instant.from(formatter.parse(value));
            } else if (Instant.class.isAssignableFrom(field.getType())) {
                return Instant.from(formatter.parse(value));
            } else if (OffsetDateTime.class.isAssignableFrom(field.getType())) {
                return OffsetDateTime.from(formatter.parse(value));
            } else if (ZonedDateTime.class.isAssignableFrom(field.getType())) {
                return ZonedDateTime.from(formatter.parse(value));
            } else if (LocalDateTime.class.isAssignableFrom(field.getType())) {
                return LocalDateTime.from(formatter.parse(value));
            } else if (LocalDate.class.isAssignableFrom(field.getType())) {
                return LocalDate.from(formatter.parse(value));
            } else if (LocalTime.class.isAssignableFrom(field.getType())) {
                return LocalTime.from(formatter.parse(value));
            } else if (OffsetTime.class.isAssignableFrom(field.getType())) {
                return OffsetTime.from(formatter.parse(value));
            }
        }

        throw new IllegalArgumentException("Conversion not supported for type " + field.getType()
                + ", use @DateFormat to supply a converter");
    }

    /**
     * Get bean metadata for the specified class.
     *
     * @param clazz The class to get metadata for.
     * @return The bean metadata.
     * @throws IllegalArgumentException If the class does not have an @ConfigurableBean annotation.
     */
    public static BeanMetadata getBeanMetadata(final Class<?> clazz) {
        final ConfigurableBean metadata = clazz.getAnnotation(ConfigurableBean.class);
        if (metadata == null) {
            throw new IllegalArgumentException("Specified class is not a @ConfigurableBean");
        }
        final BeanMetadata bean = new BeanMetadata();
        if (!metadata.name().isEmpty()) {
            bean.setName(metadata.name());
        } else {
            final char[] name = clazz.getSimpleName().toCharArray();
            name[0] = Character.toLowerCase(name[0]);
            bean.setName(new String(name));
        }
        bean.setDescription(metadata.description());
        bean.setProperties(getBeanProperties(clazz));
        return bean;
    }

    /**
     * Get bean metadata for the specified class using the specified name.
     * <p>
     * If the class implements {@link ConfigurableBean} then the description will be copied from there.  Otherwise the
     * name will be used and the class will be searched for fields with {@link ConfigurableProperty}.
     * </p>
     *
     * @param name  The name of the bean.
     * @param clazz The class of the bean.
     * @return The bean metadata.
     */
    public static BeanMetadata getBeanMetadata(final String name, final Class<?> clazz) {

        if (isConfigurableBean(clazz)) {
            final BeanMetadata metadata = getBeanMetadata(clazz);
            metadata.setName(name);
            return metadata;
        } else {
            final BeanMetadata metadata = new BeanMetadata();
            metadata.setName(name);
            metadata.setDescription("No description available");
            metadata.setProperties(getBeanProperties(clazz));
            return metadata;
        }
    }

    /**
     * Get bean properties for the specified class.
     * <p>
     * This method will ignore the {@link ConfigurableBean} annotation if it exists including any imports.  To specify
     * classes to import properties from prefer {@link #getBeanProperties(Class, Class[])} instead.
     * </p>
     *
     * @param clazz The class to get properties for.
     * @return The property metadata.
     */
    public static List<PropertyMetadata> getBeanProperties(final Class<?> clazz) {
        return getBeanProperties(clazz, new String[0]);
    }

    /**
     * Get bean properties for the specified class while also importing from the specified classes.
     * <p>
     * Useful when a class should include properties from another class without using {@link ConfigurableBean}.  Any
     * properties that are set on the class will override imported properties of the same name.
     * </p>
     *
     * @param clazz   The class to get properties for.
     * @param imports Additional classes to import properties from.
     * @return The property metadata for the class and all imported classes.
     */
    public static List<PropertyMetadata> getBeanProperties(final Class<?> clazz, final Class<?>... imports) {
        final Map<String, PropertyMetadata> properties = new HashMap<>();
        for (final Class<?> anImport : imports) {
            for (final PropertyMetadata propertyMetadata : getBeanProperties(anImport)) {
                properties.put(propertyMetadata.getName(), propertyMetadata);
            }
        }
        for (final PropertyMetadata propertyMetadata : getBeanProperties(clazz)) {
            properties.put(propertyMetadata.getName(), propertyMetadata);
        }
        return new ArrayList<>(properties.values());
    }

    /**
     * Get bean properties for the class while specifying which properties to include by name.
     *
     * @param clazz               The class to get properties from.
     * @param propertiesToInclude The name of properties to include or none to include all.
     * @return The property metadata for specified properties, or all properties if none are provided.
     */
    public static List<PropertyMetadata> getBeanProperties(final Class<?> clazz, final String... propertiesToInclude) {
        final Set<String> included = new HashSet<>(propertiesToInclude.length);
        if (propertiesToInclude.length > 0) {
            included.addAll(Arrays.asList(propertiesToInclude));
        }

        final Map<String, PropertyMetadata> properties = new HashMap<>();

        // add "imports" from the annotation
        final ConfigurableBean configurableBean = clazz.getAnnotation(ConfigurableBean.class);
        if (configurableBean != null && configurableBean.imports().length > 0) {
            for (final Class aClass : configurableBean.imports()) {
                mergeProperties(properties, getBeanProperties(aClass, propertiesToInclude));
            }
        }

        // add any from the superclass
        if (clazz.getSuperclass() != null) {
            mergeProperties(properties, getBeanProperties(clazz.getSuperclass(), propertiesToInclude));
        }

        // add fields using @ConfiguredBean
        for (final Field delegate : getFields(clazz, withAnnotation(ConfiguredBean.class))) {
            final List<PropertyMetadata> delegates = getBeanProperties(delegate.getType(), propertiesToInclude);
            delegates.forEach(d -> d.setDelegate(true));
            mergeProperties(properties, delegates);
        }

        // finally add fields from the class itself
        for (final Field field : getFields(clazz, withAnnotation(ConfigurableProperty.class))) {
            final PropertyMetadata property = getPropertyMetadata(field);
            if (propertiesToInclude.length == 0 || included.contains(property.getName())) {
                properties.put(property.getName(), property);
            }
        }

        return new ArrayList<>(properties.values());
    }

    /**
     * Map properties for the class by name.
     *
     * @param clazz               The class to get properties for.
     * @param propertiesToInclude The names of properties to include or none to include all.
     * @return The property metadata mapped by name.
     */
    public static Map<String, PropertyMetadata> mapBeanProperties(final Class<?> clazz,
                                                                  final String... propertiesToInclude) {
        return getBeanProperties(clazz, propertiesToInclude).stream().collect(toMap(PropertyMetadata::getName, p -> p));
    }

    /**
     * Get metadata for a single property.
     *
     * @param clazz    The class to scan for properties.
     * @param property The name of the property to find.
     * @return Metadata for the specified property.
     * @throws IllegalStateException If no properties are found or if more than one are found for the specified name.
     */
    public static PropertyMetadata getBeanProperty(final Class<?> clazz, final String property) {
        final List<PropertyMetadata> metadata = getBeanProperties(clazz, property);
        if (metadata.size() != 1) {
            throw new IllegalStateException("Expecting only a single property for " + property
                    + " but found " + metadata.size());
        }
        return metadata.get(0);
    }

    /**
     * Build metadata for a {@link Field}.
     *
     * @param field The field to get metadata for.
     * @return The metadata.
     * @throws IllegalArgumentException If the field is not annotated with {@link ConfigurableProperty}.
     */
    static PropertyMetadata getPropertyMetadata(final Field field) {
        final ConfigurableProperty metadata = field.getAnnotation(ConfigurableProperty.class);
        if (metadata == null) {
            throw new IllegalArgumentException("Field does not have @ConfigurableProperty annotation");
        }
        final String name = metadata.name().isEmpty() ? field.getName() : metadata.name();
        final PropertyMetadata property = new PropertyMetadata();
        property.setField(field);
        property.setName(name);
        if (!metadata.description().isEmpty()) {
            property.setDescription(metadata.description());
        }
        if (!metadata.defaultValue().equals("\n\t\t\n\t\t\n\uE000\uE001\uE002\n\t\t\t\t\n")) {
            property.setDefaultValue(metadata.defaultValue());
        } else {
            property.setRequired(metadata.required());
        }
        if (metadata.type() != Object.class) {
            property.setClazz(metadata.type());
        } else {
            property.setClazz(field.getType());
        }
        if (property.getClazz().isEnum()) {
            final Set<String> constants = Arrays.stream(property.getClazz().getEnumConstants())
                    .map(e -> ((Enum) e).name())
                    .collect(Collectors.toSet());
            property.setEnumConstants(constants);
        }
        property.setType(property.getClazz().getName());
        return property;
    }

    /**
     * Merge a {@link List} of properties with an existing {@link Map} of properties.
     * <p>
     * For each property in {@code propertiesToImport} if the property is already in the map it will be merged using
     * {@link #mergeProperty(PropertyMetadata, PropertyMetadata)}.  If it is not in the map then it will be added.
     * </p>
     *
     * @param properties         The existing properties to merge with.
     * @param propertiesToImport The new properties to merge into the existing.
     */
    private static void mergeProperties(final Map<String, PropertyMetadata> properties,
                                        final List<PropertyMetadata> propertiesToImport) {
        propertiesToImport.forEach(p -> {
            if (properties.containsKey(p.getName())) {
                mergeProperty(properties.get(p.getName()), p);
            } else {
                properties.put(p.getName(), p);
            }
        });
    }

    /**
     * Merge a property by replacing values in {@code original} with those in {@code override} where available.
     *
     * @param original The original property.
     * @param override The override to merge with the original.
     */
    private static void mergeProperty(final PropertyMetadata original, final PropertyMetadata override) {
        if (override.getName() != null && !override.getName().isEmpty()) {
            original.setName(override.getName());
        }
        if (override.getDescription() != null && !override.getDescription().isEmpty()) {
            original.setDescription(override.getDescription());
        }
        if (override.getDefaultValue() != null && !override.getDefaultValue().isEmpty()) {
            original.setDefaultValue(override.getDefaultValue());
        } else if (override.isRequired()) {
            original.setRequired(true);
        }
        original.setField(override.getField());
        original.setClazz(override.getClazz());
        original.setType(override.getType());
    }

    /**
     * Test if a class is a {@link ConfigurableBean}.
     *
     * @param clazz The class to check.
     * @return True if it is a configurable bean (the annotation is present), false if not.
     */
    public static boolean isConfigurableBean(final Class<?> clazz) {
        return clazz.getAnnotation(ConfigurableBean.class) != null;
    }
}
