/*
 * Copyright 2018 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans.fixtures;

import net.kenblair.beans.annotations.ConfigurableProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Short.parseShort;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Fixture for testing type conversion.
 */
public class BeanOfManyTypes {

    public static enum AnEnum {
        FOO,
        BAR
    }

    @ConfigurableProperty
    private Integer testInteger;
    @ConfigurableProperty
    private Long testLong;
    @ConfigurableProperty
    private Float testFloat;
    @ConfigurableProperty
    private Double testDouble;
    @ConfigurableProperty
    private Short testShort;
    @ConfigurableProperty
    private Character testChar;
    @ConfigurableProperty
    private Boolean testBoolean;
    @ConfigurableProperty
    private int primitiveInt;
    @ConfigurableProperty
    private long primitiveLong;
    @ConfigurableProperty
    private float primitiveFloat;
    @ConfigurableProperty
    private double primitiveDouble;
    @ConfigurableProperty
    private short primitiveShort;
    @ConfigurableProperty
    private char primitiveChar;
    @ConfigurableProperty
    private boolean primitiveBoolean;
    @ConfigurableProperty
    private AnEnum singleEnum;
    @ConfigurableProperty
    private Date date;
    @ConfigurableProperty
    private Instant instant;
    @ConfigurableProperty
    private OffsetDateTime offsetDateTime;

    public Map<String, Object> getTestProperties() {
        final Map<String, Object> properties = new HashMap<>(15);
        properties.put("testInteger", 123);
        properties.put("primitiveInt", 123);
        properties.put("testLong", 1234567890L);
        properties.put("primitiveLong", 1234567890L);
        properties.put("testFloat", 123.4F);
        properties.put("primitiveFloat", 123.4F);
        properties.put("testDouble", 123.456);
        properties.put("primitiveDouble", 123.456);
        properties.put("testShort", parseShort("321"));
        properties.put("primitiveShort", parseShort("321"));
        properties.put("testBoolean", true);
        properties.put("primitiveBoolean", true);
        properties.put("testChar", 'C');
        properties.put("primitiveChar", 'C');
        properties.put("singleEnum", AnEnum.FOO.name());
        properties.put("date", "2011-12-03T10:15:30");
        properties.put("instant", "2011-12-03T10:15:30");
        properties.put("offsetDateTime", "2011-12-03T10:15:30+05:00");
        return properties;
    }

    public void validateProperties() {
        assertThat(this.testInteger).isEqualTo(123);
        assertThat(this.primitiveInt).isEqualTo(123);
        assertThat(this.testLong).isEqualTo(1234567890L);
        assertThat(this.primitiveLong).isEqualTo(1234567890L);
        assertThat(this.testFloat).isEqualTo(123.4F);
        assertThat(this.primitiveFloat).isEqualTo(123.4F);
        assertThat(this.testDouble).isEqualTo(123.456);
        assertThat(this.primitiveDouble).isEqualTo(123.456);
        assertThat(this.testShort).isEqualTo(parseShort("321"));
        assertThat(this.primitiveShort).isEqualTo(parseShort("321"));
        assertThat(this.testBoolean).isEqualTo(true);
        assertThat(this.primitiveBoolean).isEqualTo(true);
        assertThat(this.testChar).isEqualTo('C');
        assertThat(this.primitiveChar).isEqualTo('C');
        assertThat(this.singleEnum).isEqualTo(AnEnum.FOO);

        final Instant expectedTime = Instant.from(DateTimeFormatter.ISO_DATE_TIME.parse("2011-12-03T10:15:30+00:00"));
        assertThat(this.date).isEqualTo(Date.from(expectedTime));
        assertThat(this.instant).isEqualTo(expectedTime);
        assertThat(this.offsetDateTime).isEqualTo(expectedTime.atOffset(ZoneOffset.ofHours(5)).minusHours(5));
    }
}
