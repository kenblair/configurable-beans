/*
 * Copyright 2015 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to document metadata on configurable properties for an object.
 *
 * @author kblair
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConfigurableProperty {

    /**
     * The name of the property, should match the field name.
     *
     * @return The name.
     */
    String name() default "";

    /**
     * A description of the property.
     *
     * @return The description.
     */
    String description() default "";

    /**
     * The default value for the property, if it has one.
     * <p>
     * If a default value is set then the property is implicitly not required.
     * </p>
     *
     * @return The default value.
     */
    String defaultValue() default "\n\t\t\n\t\t\n\uE000\uE001\uE002\n\t\t\t\t\n";

    /**
     * True if the property is required, false if it is optional.
     * <p>
     * Properties with a default value are implicitly not required.
     * </p>
     *
     * @return True if required, false if not.
     */
    boolean required() default true;

    /**
     * The expected type of the property.
     *
     * @return The type.
     */
    Class<?> type() default Object.class;
}
