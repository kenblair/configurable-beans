package net.kenblair.beans.fixtures;

import net.kenblair.beans.annotations.ConfigurableBean;
import net.kenblair.beans.annotations.ConfigurableProperty;

@ConfigurableBean(description = "Imports properties from another bean", imports = ImportingBean.BeanToImportFrom.class)
public class ImportingBean {

    @ConfigurableProperty(description = "Base property")
    private String baseProperty;

    @ConfigurableProperty(description = "Override the imported property")
    private String overriddenProperty;

    public static class BeanToImportFrom {

        @ConfigurableProperty(description = "This is an imported property")
        private String importedProperty;

        @ConfigurableProperty(description = "Should get overridden")
        private String overriddenProperty;

    }
}
