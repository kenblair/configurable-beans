/*
 * Copyright 2015 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans;


import net.kenblair.beans.annotations.ConfigurableBean;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static net.kenblair.beans.ConfigurableBeans.getBeanMetadata;

/**
 * Scan classes or packages for {@link ConfigurableBean} annotations to provide {@link BeanMetadata} using
 * {@link Reflections}.
 *
 * @author kblair
 */
public class ReflectionsBeanScanner {

    private final Reflections reflections;

    /**
     * Scan for beans starting with the package of {@code baseClass}.
     * <p>
     * Sub-packages will also be scanned.
     * </p>
     *
     * @param baseClass A class in the package to start scanning in.
     */
    public ReflectionsBeanScanner(final Class<?> baseClass) {
        this(baseClass.getPackage().getName());
    }

    /**
     * Scan for beans starting with the specified package.
     * <p>
     * Sub-packages will also be scanned.
     * </p>
     *
     * @param basePackage The package to start with.
     */
    public ReflectionsBeanScanner(final String basePackage) {
        this(new Reflections(basePackage));
    }

    /**
     * Scan for beans using the provided {@link Reflections} instance.
     *
     * @param reflections The instance to use.
     */
    private ReflectionsBeanScanner(final Reflections reflections) {
        if (reflections == null) {
            throw new IllegalArgumentException("reflections must not be null");
        }
        this.reflections = reflections;
    }

    /**
     * Get all configurable beans.
     *
     * @return Metadata for all found beans.
     */
    public List<BeanMetadata> getBeans() {
        return scan();
    }

    /**
     * Get all configurable beans of the specified type.
     *
     * @param type The type of bean.
     * @return Metadata for beans of the specified type.
     */
    public <T> List<BeanMetadata> getBeans(final Class<T> type) {
        final List<BeanMetadata> metadata = new ArrayList<>();
        final Set<Class<? extends T>> classes = reflections.getSubTypesOf(type);
        for (final Class<? extends T> aClass : classes) {
            if (aClass.getAnnotation(ConfigurableBean.class) != null) {
                metadata.add(getBeanMetadata(aClass));
            }
        }
        return metadata;
    }

    /**
     * Get a configurable bean with the specified name.
     *
     * @param name The name of the bean to find.
     * @return Optional with metadata for the bean with the specified name if it is found.
     */
    public Optional<BeanMetadata> getBean(final String name) {
        if (name == null) {
            throw new IllegalArgumentException("name may not be null");
        }
        for (final Class<?> bean : reflections.getTypesAnnotatedWith(ConfigurableBean.class)) {
            final BeanMetadata metadata = getBeanMetadata(bean);
            if (name.equals(metadata.getName())) {
                return Optional.of(metadata);
            }
        }
        return Optional.empty();
    }

    private List<BeanMetadata> scan() {
        final List<BeanMetadata> metadata = new ArrayList<>();
        for (final Class<?> bean : reflections.getTypesAnnotatedWith(ConfigurableBean.class)) {
            metadata.add(getBeanMetadata(bean));
        }
        return metadata;
    }

}
