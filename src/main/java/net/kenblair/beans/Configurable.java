/*
 * Copyright 2018 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans;

/**
 * Indicates a class that may be configured using a specified configuration class.
 * <p>
 * If the configuration class cannot be discovered at runtime, such as if no parameterized type is provided, then
 * a {@code Map<String, Object>} will be passed instead.
 * </p>
 *
 * @param <E> The type of configuration accepted.
 * @author Ken Blair
 */
public interface Configurable<E> {

    /**
     * Configure using the specified configuration.
     *
     * @param configuration The configuration to use.
     */
    void configure(E configuration);

}
