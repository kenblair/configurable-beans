# configurable-beans

Document and configure properties for Java beans.

Currently only available as a set of static functions in ConfigurableBeans.

## Bean Configuration

Beans can be configured using the ConfigurableBean annotation.  Beans are expected to have a name, a description and a set of properties.

    @ConfigurableBean(description = "Sends a personalized message.")
    public class PersonalMessage {
    
        @ConfigurableProperty(description = "A name to use in the message.")
        private String name;
        
        @ConfigurableProperty(name = "msg", description = "The message to send using {name} as a template for the name.", defaultValue="Hello {name}!")
        private String message;
    }
    
In the above example the PersonalMessage bean supports two properties: name and message.

The "name" property is required because it does not have a default value and is not marked explicitly as not required.  As no name was specified in the annotation it will use the name of the field.

Message uses the name "msg" instead of the field name of "message" and is not required because a default value is provided.

### Documentation

Extract BeanMetadata like so:

    BeanMetadata beanMetadata = ConfigurableBeans.getBeanMetadata(PersonalMessage.class);
    System.out.println("Bean name: " + beanMetadata.getName() + 
                       ", description: " + beanMetadata.getDescription());
    for (PropertyMetadata property : beanMetadata.getProperties()) {
        System.out.println("Property name: " + property.getName() + 
                           ", description: " + property.getDescription() + 
                           ", class: " + property.getClazz() + 
                           ", required: " + property.isRequired() + 
                           ", defaultValue: " + property.getDefaultValue());
    }
    
### Injection

Configure the bean using a Map or Configuration with all required values.

    Map<String, Object> properties = new HashMap<>(2);
    properties.put("name", "Bob");
    properties.put("msg", "Goodbye {name}."
    
    PersonalMessage message = new PersonalMessage();
    ConfigurableBeans.configure(message, properties);
    
If any of the required properties are not set an IllegalArgumentException will be thrown.  If the property provided cannot be converted to the appropriate type an IllegalArgumentException will be thrown.  In either case the exception message should describe the problem.

By default properties will be injected on the bean using reflections.  If the field is not accessible it will be made accessible and then returned to it's previous state.  Final fields are not supported and will result in an IllegalArgumentException.  

ONLY fields annotated with @ConfigurableProperty will be injected.

### Subclasses

Configurable beans works with subclasses.  Fields annotated with @ConfigurableProperty in a superclass will be included in metadata for subclasses and will be injected as normal.  A subclass may "override" a property by annotating a field with @ConfigurableProperty using the same name.

    public class NamedPersonalMessage extends PersonalMessage {
        
        @ConfigurableProperty(defaultValue = "Bob")
        private String name;
        
        @ConfigurableProperty(name = "msg", required = true)
        private String requiredMessage;
    } 
    
Configuration in the subclass will "merge" with information from the superclass.  The bean name and description will come from the superclass.  Both properties "name" and "msg" will have their description copied from the superclass but name will now have a default value and thus not be required.  Msg changes to required.

### Imports

A bean may specify that it's configuration includes the configuration of another class.  For example:

    @ConfigurableBean(description = "Sends a personal message.", imports = PersonalMessage.class)
    public class MessageSender {
    }
    
When collecting metadata on MessageSender all of the properties from PersonalMessage will be included.  Any properties in MessageSender with the same name will merge just as they do for a subclass.

Imported fields cannot be injected.  In the future you will be able to specify an @ConfiguredBean of type "PersonalMessage" and have it injected.   For now you must use Configurable or repeat the properties in each class that uses them.

### Custom Configuration

Instead of using injection a bean may specify that it should receive a configuration object:

    @ConfigurableBean(description = "Sends a personalized message.")
    public class PersonalMessage implements Configurable<Map<String, Object>> {
            
        @ConfigurableProperty(description = "A name to use in the message.")
        private String name;
        
        @ConfigurableProperty(name = "msg", description = "The message to send using {name} as a template for the name.", defaultValue="Hello {name}!")
        private String message;
            
        @Override
        public void configure(Map<String, Object> properties) {
            this.name = properties.get("name");
            this.message = properties.get("msg");
        }
    }
    
Custom configuration works with a Map, a commons Configuration or ImmutableConfiguration, and with any class that has a no-argument constructor and @ConfigurableProperty annotated on it's fields.

    @ConfigurableBean(description = "Sends a personal message.", imports = PersonalMessage.class)
    public class MessageSender implements Configurable<PersonalMessage> {
        
        @Override
        public void configure(PersonalMessage message) {
            sendMessage(message.getName(), message.getMessage());
        }
    }
        
As a result a custom configuration can be stored and documented in a separate class, or a static inner class.

    @ConfigurableBean(description = "Sends a personal message.", imports = MessageSenderConfiguration.class)
    public class MessageSender implements Configurable<MessageSenderConfiguration> {
        private MessageSenderConfiguration config;
        
        @Override
        public void configure(MessageSenderConfiguration configuration) {
            sendMessage(configuration.getName(), configuration.getMessage());
        }
        
        public void sendMessage() {
            service.send(config.getMessage().replaceAll("{name}", config.getName()));
        }
    }
    
The advantage being the configuration can now include additional validation and may be used to setup the configuration to avoid using keys or worrying about type conversions.

    public class MessageSenderConfiguration {
        @ConfigurableProperty(name = "msg", description = "A named message to send using {name} template.", defaultValue = "Hello {name}!")
        private String message;
        
        @ConfigurableProperty(description = "The name to send the message to.")
        private String name;
        
        public String getName() {
            return this.name;
        }
        
        public String getMessage() {
            return this.message;
        }
        
        public MessageSenderConfiguration withName(String name) {
            this.name = name;
        }
        
        public MessageSenderConfiguration withMessage(String message) {
            this.message = message;
        }
    }
    
Now MessageSender can be configured with type safety, either through code directly...

    sender.configure(new MessageSenderConfiguration().withName("Samantha").withMessage("How are you {name}?")).sendMessage();
    
...or with configurable beans.

    ConfigurableBeans.configure(sender, new MessageSenderConfiguration().withName("Johnny"));
    sender.sendMessage(); 
    
Additionally the values can be extracted, such as for sending to another API or service.

    Map<String, Object> properties = ConfigurableBeans.buildPropertiesFrom(new MessageSenderConfiguration().withName("Sarah"));    
    properties.get("name"); // returns "Sarah"
    properties.get("msg"); // returns the default of "Hello {name}!"

## Property Configuration

Properties can be documented and configured without using @ConfigurableBean.  Any class can have one or more fields annotated with @ConfigurableProperty and they can be retrieved with ConfigurableBeans.getBeanProperties().

    public class OneThing {
        private String foo;
        private Date bar;
        
        @ConfigurableProperty(name = "count", description = "Number of times to do the one thing.", defaultValue = 10)
        private int configureThis;
    }
    
The properties foo and bar are ignored, only the field "configureThis" will be documented or injected and it will be done using "count" as the key.