package net.kenblair.beans.fixtures;

import net.kenblair.beans.annotations.ConfigurableProperty;

public class ChildBean extends ParentBean {

    public static final String CHILD_PROPERTY_DESC = "A property on the child";

    @ConfigurableProperty(description = CHILD_PROPERTY_DESC)
    private String childProperty;
}
