/*
 * Copyright 2018 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans;

/**
 * Interface for classes that wish to be initialized after configuration.
 * <p>
 * Occasionally a class may need to perform initialization *after* configuration has occurred such as through field
 * injection or using {@link Configurable}.  When {@link Initializable} is implemented the {@link #initialize()} method
 * will be called once Configurable Beans has configured/injected the bean.  Implementations should make a best effort
 * to <b>not</b> throw an exception and to instead handle any errors during initialization.
 * </p>
 * <p>
 * If {@link #initialize()} throws an exception for any reason then {@link #initializationFailure(Exception)} will be
 * called with the caught {@link Exception}.  Generally this functionality should be avoided as {@link #initialize()}
 * should make a best effort to handle an exception rather than throwing it.  In cases where this is not feasible an
 * implementation may instead elect to throw an exception and handle the failure through
 * {@link #initializationFailure(Exception)} instead.
 * </p>
 *
 * @author kblair
 */
public interface Initializable {

    /**
     * Perform initialization after properties have been configured.
     * <p>
     * Implementations should avoid throwing an exception where possible.  If an exception is thrown then
     * {@link #initializationFailure(Exception)} must be called with the exception.
     * </p>
     *
     * @throws Exception The exception thrown during initialization.
     */
    void initialize() throws Exception;

    /**
     * Handle initialization failure for the provided {@link Exception}.
     * <p>
     * Called when {@link #initialize()} throws an exception.
     * </p>
     *
     * @param ex The exception thrown during initialization.
     */
    default void initializationFailure(Exception ex) {
        // no-op by default
    }
}
