/*
 * Copyright 2015 Ken Blair
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kenblair.beans.fixtures;

import net.kenblair.beans.annotations.ConfigurableBean;
import net.kenblair.beans.annotations.ConfigurableProperty;

/**
 * @author kblair
 */
@ConfigurableBean(name = "Counter", description = "Counts for you.")
public class CountingBean implements Runnable {

    @ConfigurableProperty(description = "The number to start counting from.", defaultValue = "0")
    private int start = 0;

    @ConfigurableProperty(description = "The number to stop counting at.", defaultValue = "100")
    private int end = 100;

    @ConfigurableProperty(name = "incr", description = "How much to increment by each count.", required = true)
    private int increment;

    @ConfigurableProperty(description = "Delay between each count in milliseconds.", defaultValue = "100")
    private long delay = 100;

    @Override
    public void run() {
        for (int i = start; i <= end; i += increment) {
            System.out.println(i);
            try {
                Thread.sleep(delay);
            } catch (final InterruptedException e) {
                break;
            }
        }
    }

    public int getStart() {
        return start;
    }

    public void setStart(final int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(final int end) {
        this.end = end;
    }

    public int getIncr() {
        return increment;
    }

    public void setIncr(final int increment) {
        this.increment = increment;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(final long delay) {
        this.delay = delay;
    }
}
