package net.kenblair.beans;

import java.time.temporal.TemporalAccessor;

public interface CustomDateFormat {

    TemporalAccessor parse(String value);

    String getFormat();
}
