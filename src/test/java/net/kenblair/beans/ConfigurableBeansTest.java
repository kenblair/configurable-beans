package net.kenblair.beans;

import net.kenblair.beans.annotations.ConfigurableProperty;
import net.kenblair.beans.fixtures.BeanOfManyTypes;
import net.kenblair.beans.fixtures.ChildBean;
import net.kenblair.beans.fixtures.CountingBean;
import net.kenblair.beans.fixtures.ImportingBean;
import net.kenblair.beans.fixtures.ParentBean;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.junit.Test;

import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for {@link ConfigurableBeans}.
 */
public class ConfigurableBeansTest {

    @Test
    public void configure() {
        final ConfigureBean configureBean = new ConfigureBean();
        final Map<String, Object> properties = new HashMap<>(4);
        properties.put("testProperty1", "test");
        properties.put("testProperty2", true);

        ConfigurableBeans.configure(configureBean, properties);

        assertThat(configureBean.testProperty1).isEqualTo("test");
        assertThat(configureBean.testProperty2).isTrue();
        assertThat(configureBean.testProperty3).isEqualTo(100);

        final CountingBean countingBean = new CountingBean();
        final Map<String, Object> props = new HashMap<>(4);
        props.put("start", 10);
        props.put("end", 50);
        props.put("incr", 2);
        props.put("delay", 500);

        ConfigurableBeans.configure(countingBean, props);

        assertThat(countingBean.getStart()).isEqualTo(10);
        assertThat(countingBean.getEnd()).isEqualTo(50);
        assertThat(countingBean.getIncr()).isEqualTo(2);
        assertThat(countingBean.getDelay()).isEqualTo(500);
    }

    @Test
    public void configureBean() {
        final ConfigureBean bean = new ConfigureBean();
        final Map<String, Object> properties = new HashMap<>(2);
        properties.put("testProperty1", "test");
        properties.put("subProperty", "foo");

        ConfigurableBeans.configureBean(bean, properties);

        assertThat(bean.testProperty1).isEqualTo("test");
        assertThat(bean.testProperty2).isTrue();
        assertThat(bean.testProperty3).isEqualTo(100);
        assertThat(bean.subProperty).isEqualTo("foo");
    }

    @Test
    public void configureBean_CommonsConfiguration() {
        final CommonsConfigBean bean = new CommonsConfigBean();
        final Map<String, Object> properties = new HashMap<>(2);
        properties.put("testProperty1", "test");
        properties.put("throwException", false);

        ConfigurableBeans.configureBean(bean, properties);

        assertThat(bean.testProperty1).isEqualTo("test");
        assertThat(bean.testProperty2).isEqualTo("foobar");
    }

    @Test
    public void configureBean_CommonsConfiguration2() {
        final CommonsConfig2Bean bean = new CommonsConfig2Bean();
        final Map<String, Object> properties = new HashMap<>(1);
        properties.put("testProperty1", "test");

        ConfigurableBeans.configureBean(bean, properties);

        assertThat(bean.testProperty1).isEqualTo("test");
    }

    @Test
    public void configureBean_ImmutableConfiguration() {
        final ImmutableConfigBean bean = new ImmutableConfigBean();
        final Map<String, Object> properties = new HashMap<>(1);
        properties.put("testProperty1", "test");

        ConfigurableBeans.configureBean(bean, properties);

        assertThat(bean.testProperty1).isEqualTo("test");
    }

    @Test
    public void initializeBean() {
        final InitializingBean bean = new InitializingBean();
        final InitializingBean spy = spy(bean);

        ConfigurableBeans.configure(spy, Collections.emptyMap());

        verify(spy, times(1)).initialize();
        verify(spy, times(0)).initializationFailure(any());
    }

    @Test
    public void initializeBean_withConfiguration() {
        final CommonsConfigBean bean = spy(new CommonsConfigBean());
        final Map<String, Object> properties = new HashMap<>(2);
        properties.put("testProperty1", "test");
        properties.put("throwException", false);

        ConfigurableBeans.configureBean(bean, properties);

        assertThat(bean.testProperty1).isEqualTo("test");
        assertThat(bean.testProperty2).isEqualTo("foobar");

        verify(bean, times(1)).initialize();
        verify(bean, times(0)).initializationFailure(any());
    }

    @Test(expected = RuntimeException.class)
    public void initializeBean_withFailure() {
        final InitializingBean bean = spy(new InitializingBean());
        final Map<String, Object> properties = new HashMap<>(1);
        properties.put("shouldThrowException", true);

        ConfigurableBeans.configure(bean, properties);
    }

    @Test(expected = IllegalStateException.class)
    public void initializeBean_withHandleFailure() {
        final InitializingBean bean = new InitializingBean() {
            @Override
            public void initializationFailure(Exception ex) {
                throw new IllegalStateException("because");
            }
        };
        final Map<String, Object> properties = new HashMap<>(1);
        properties.put("shouldThrowException", true);

        ConfigurableBeans.configure(bean, properties);
    }

    @Test
    public void testTypeConversions() throws ParseException {
        final BeanOfManyTypes bean = new BeanOfManyTypes();
        ConfigurableBeans.injectBean(bean, bean.getTestProperties());
        bean.validateProperties();
    }

    @Test
    public void injectBean() {
        final CountingBean bean = new CountingBean();
        final Map<String, Object> properties = new HashMap<>(4);
        properties.put("start", 10);
        properties.put("end", 50);
        properties.put("incr", 2);
        properties.put("delay", 500);

        ConfigurableBeans.injectBean(bean, properties);

        assertThat(bean.getStart()).isEqualTo(10);
        assertThat(bean.getEnd()).isEqualTo(50);
        assertThat(bean.getIncr()).isEqualTo(2);
        assertThat(bean.getDelay()).isEqualTo(500);
    }

    @Test
    public void getBeanProperties() {
        final List<PropertyMetadata> properties = ConfigurableBeans.getBeanProperties(CountingBean.class);
        validateCounterProperties(properties);
    }

    @Test
    public void getBeanProperties_HierarchicalBean() {
        final Map<String, PropertyMetadata> nameToPropertyMap = ConfigurableBeans.mapBeanProperties(ChildBean.class);
        assertThat(nameToPropertyMap.size()).isEqualTo(2);
        assertThat(nameToPropertyMap.get("name").getDescription()).isEqualTo(ParentBean.NAME_DESC);
        assertThat(nameToPropertyMap.get("childProperty").getDescription()).isEqualTo(ChildBean.CHILD_PROPERTY_DESC);
    }

    @Test
    public void testEnumConstants() {
        final Map<String, PropertyMetadata> properties = buildNameMap(ConfigurableBeans.getBeanProperties(BeanOfManyTypes.class));
        assertThat(properties.get("singleEnum").getEnumConstants()).containsExactlyInAnyOrder("FOO", "BAR");
    }

    @Test
    public void getBeanMetadata() {
        final BeanMetadata bean = ConfigurableBeans.getBeanMetadata(CountingBean.class);
        assertThat(bean.getName()).isEqualTo("Counter");
        assertThat(bean.getDescription()).isEqualTo("Counts for you.");

        validateCounterProperties(bean.getProperties());
    }

    @Test
    public void getBeanMetadata_withImport() {
        final BeanMetadata bean = ConfigurableBeans.getBeanMetadata(ImportingBean.class);

        assertThat(bean.getName()).isEqualTo("importingBean");
        assertThat(bean.getDescription()).isEqualTo("Imports properties from another bean");
        assertThat(bean.getProperties().size()).isEqualTo(3);

        final Map<String, PropertyMetadata> propertyMap = buildNameMap(bean.getProperties());
        assertThat(propertyMap.get("baseProperty")).isNotNull();
        assertThat(propertyMap.get("baseProperty").getDescription()).isEqualTo("Base property");
        assertThat(propertyMap.get("overriddenProperty")).isNotNull();
        assertThat(propertyMap.get("overriddenProperty").getDescription()).isEqualTo("Override the imported property");
        assertThat(propertyMap.get("importedProperty")).isNotNull();
        assertThat(propertyMap.get("importedProperty").getDescription()).isEqualTo("This is an imported property");

    }

    private void validateCounterProperties(final List<PropertyMetadata> properties) {
        final Map<String, PropertyMetadata> nameToPropertyMap = buildNameMap(properties);

        final PropertyMetadata start = nameToPropertyMap.get("start");
        assertNotNull("Bean did not have expected name of 'start'.", start);
        assertThat(start.getDescription()).isEqualTo("The number to start counting from.");
        assertThat(start.getDefaultValue()).isEqualTo("0");
        assertThat(start.isRequired()).isFalse();
        assertThat(start.getClazz()).isEqualTo(Integer.TYPE);
        assertThat(start.getType()).isEqualTo(Integer.TYPE.getName());

        final PropertyMetadata end = nameToPropertyMap.get("end");
        assertNotNull("Bean did not have expected name of 'end'.", end);
        assertThat(end.getDescription()).isEqualTo("The number to stop counting at.");
        assertThat(end.getDefaultValue()).isEqualTo("100");
        assertThat(end.isRequired()).isFalse();
        assertThat(end.getClazz()).isEqualTo(Integer.TYPE);
        assertThat(end.getType()).isEqualTo(Integer.TYPE.getName());

        final PropertyMetadata increment = nameToPropertyMap.get("incr");
        assertNotNull("Bean did not have expected name of 'incr'.", increment);
        assertThat(increment.getDescription()).isEqualTo("How much to increment by each count.");
        assertThat(increment.getDefaultValue()).isNullOrEmpty();
        assertThat(increment.isRequired()).isTrue();
        assertThat(increment.getClazz()).isEqualTo(Integer.TYPE);
        assertThat(increment.getType()).isEqualTo(Integer.TYPE.getName());

        final PropertyMetadata delay = nameToPropertyMap.get("delay");
        assertNotNull("Bean did not have expected name of 'delay'.", delay);
        assertThat(delay.getDescription()).isEqualTo("Delay between each count in milliseconds.");
        assertThat(delay.getDefaultValue()).isEqualTo("100");
        assertThat(delay.isRequired()).isFalse();
        assertThat(delay.getClazz()).isEqualTo(Long.TYPE);
        assertThat(delay.getType()).isEqualTo(Long.TYPE.getName());
    }

    private Map<String, PropertyMetadata> buildNameMap(final List<PropertyMetadata> properties) {
        return properties.stream().collect(toMap(PropertyMetadata::getName, p -> p));
    }

    /**
     * A bean that is initializable.
     */
    private static class InitializingBean implements Initializable {

        @ConfigurableProperty(description = "A property that should be injected", defaultValue = "false")
        private boolean shouldThrowException;

        @Override
        public void initialize() {
            if (shouldThrowException) {
                throw new IllegalStateException("I was told to do this");
            }
        }
    }

    /**
     * A bean that expects Commons Configuration and is initializable.
     */
    private static class CommonsConfigBean implements Configurable<Configuration>, Initializable {
        @ConfigurableProperty(description = "A test property")
        private String testProperty1;

        @ConfigurableProperty(description = "A property with default value", defaultValue = "foobar")
        private String testProperty2;

        @ConfigurableProperty(description = "To throw or not to throw")
        private boolean throwException;

        @Override
        public void configure(final Configuration configuration) {
            this.testProperty1 = configuration.getString("testProperty1");
            this.testProperty2 = configuration.getString("testProperty2");
            this.throwException = configuration.getBoolean("throwException");
        }

        @Override
        public void initialize() {
            if (throwException) {
                throw new IllegalStateException("I was told to do this");
            }
        }

    }

    /**
     * A bean that expects Commons Configuration.
     */
    private static class CommonsConfig2Bean implements Configurable<org.apache.commons.configuration2.Configuration> {
        @ConfigurableProperty(description = "A test property")
        private String testProperty1;

        @Override
        public void configure(final org.apache.commons.configuration2.Configuration configuration) {
            this.testProperty1 = configuration.getString("testProperty1");
        }
    }

    /**
     * A bean that expects Commons Configuration.
     */
    private static class ImmutableConfigBean implements Configurable<ImmutableConfiguration> {
        @ConfigurableProperty(description = "A test property")
        private String testProperty1;

        @Override
        public void configure(final ImmutableConfiguration configuration) {
            this.testProperty1 = configuration.getString("testProperty1");
        }
    }

    /**
     * A bean that uses a configuration class and Configurable.
     */
    private static class ConfigureBean implements Configurable<ConfigureBean.SubConfiguration> {

        public static class MyConfiguration {
            @ConfigurableProperty(description = "A test property")
            String testProperty1;

            @ConfigurableProperty(description = "Another test property")
            boolean testProperty2;

            @ConfigurableProperty(defaultValue = "100")
            int testPropertyWithDef;
        }

        public static class SubConfiguration extends MyConfiguration {

            @ConfigurableProperty(defaultValue = "A sub property")
            String subProperty;

            @ConfigurableProperty(defaultValue = "true")
            boolean testProperty2;
        }

        private String testProperty1;
        private boolean testProperty2;
        private int testProperty3;
        private String subProperty;

        @Override
        public void configure(SubConfiguration configuration) {
            this.testProperty1 = configuration.testProperty1;
            this.testProperty2 = configuration.testProperty2;
            this.testProperty3 = configuration.testPropertyWithDef;
            this.subProperty = configuration.subProperty;
        }
    }

}